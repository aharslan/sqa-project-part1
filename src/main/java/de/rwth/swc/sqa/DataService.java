package de.rwth.swc.sqa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import de.rwth.swc.sqa.model.Ticket;

public class DataService {
	public static  List<Customer> customerList = new ArrayList<Customer>();
	public static Map<Long, List<DiscountCard>> discountCardMap = new HashMap<Long, List<DiscountCard>>();
	public static List<Ticket> ticketList = new ArrayList<Ticket>();
	
}
