
package de.rwth.swc.sqa.api;

//import java.net.http.HttpResponse;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import de.rwth.swc.sqa.DataService;
import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import io.swagger.annotations.ApiParam;


@Controller
@RequestMapping("/customers")
public class CustomerController implements CustomersApi{
	


    @PostMapping("")
    public ResponseEntity<Customer> addCustomer(@ApiParam(value = "Customer object that should be added",required = true) @Valid @RequestBody Customer body) {
    	 if(body.getId()!=null || body.getBirthdate().equals("") ) {
    		 return ResponseEntity.status(400).body(null); 
    	 }else if(body.getDisabled()==null) {
    		 body.setDisabled(false);
    	 }
    	 body.setId(System.currentTimeMillis());
    	 DataService.customerList.add(body);
         return ResponseEntity.status(201).body(body);
    }

    @PostMapping("/{customerId}/discountcards")
    public ResponseEntity<DiscountCard> addDiscountCardToCustomer(@ApiParam(value = "ID of customer",required = true) @PathVariable("customerId") Long customerId, @ApiParam(value = "DiscountCard object that needs to be added to the customer",required = true) @RequestBody @Valid DiscountCard body) {
   	 if(body.getId()==null) {
   		 return ResponseEntity.status(400).body(null); 
   	 }
   	 boolean exists = false;
   	 for(Customer customer: DataService.customerList) {
   		 if(customer.getId().longValue()==customerId.longValue()) {
   			exists = true;
   		 }
   	 }
   	 if(!exists) {
   		 return ResponseEntity.status(404).body(null); 
   	 }
   	 boolean conflict=false;
   	 if( DataService.discountCardMap.containsKey(customerId)) {
   		List<DiscountCard> discountCardList =  DataService.discountCardMap.get(customerId);
   		for(DiscountCard card:discountCardList) {
   			
   			if(card.getValidFrom().equals(body.getValidFrom())) {
   				conflict = true;
   			}
   		}
   		if(conflict) {
   			return ResponseEntity.status(409).body(null); 
   		}else {
   			discountCardList.add(body);
   		}
   	 }else {
   		List<DiscountCard> discountCardList = new ArrayList<DiscountCard>();
   		discountCardList.add(body);
   		DataService.discountCardMap.put(customerId, discountCardList);
   	 }
        return ResponseEntity.status(201).body(body);
    }

    @GetMapping("/{customerId}/discountcards")
    public ResponseEntity<List<DiscountCard>> getCustomerDiscountCards(@ApiParam(value = "ID of customer to search for discount cards",required = true) @PathVariable("customerId") Long customerId) {
    	 if(customerId.longValue()==0) {
       		 return ResponseEntity.status(400).body(null); 
       	 }
    	 boolean exists = false;
       	 for(Customer customer: DataService.customerList) {
       		 if(customer.getId().longValue()==customerId.longValue() &&  DataService.discountCardMap.containsKey(customerId)) {
       			exists = true;
       		 }
       	 }
       	 if(!exists) {
       		 return ResponseEntity.status(404).body(null); 
       	 }
        return ResponseEntity.ok().body( DataService.discountCardMap.get(customerId));
    }

}
