package de.rwth.swc.sqa.api;

import java.text.SimpleDateFormat;
import java.util.List;

import javax.validation.Valid;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import de.rwth.swc.sqa.DataService;
import de.rwth.swc.sqa.model.Ticket;
import de.rwth.swc.sqa.model.TicketRequest;
import de.rwth.swc.sqa.model.TicketValidationRequest;
import io.swagger.annotations.ApiParam;

@Controller
@RequestMapping("/tickets")
public class TicketController implements TicketsApi{

    @PostMapping("")
    public ResponseEntity<Ticket> buyTicket(@ApiParam(value = "TicketRequest object" ,required=true )  @Valid @RequestBody TicketRequest body) {
    	try {
	    	Ticket ticket = new Ticket();
	    	ticket.setId(System.currentTimeMillis());
	    	ticket.setBirthdate(body.getBirthdate());
	    	
	    	ticket.setDisabled(body.getDisabled()==null?false:body.getDisabled());
	    	ticket.setDiscountCard(body.getDiscountCard()==null?false:body.getDiscountCard());
	    	ticket.setStudent(body.getStudent()==null?false:body.getStudent());
	    	ticket.setValidFor(Ticket.ValidForEnum.fromValue(body.getValidFor().getValue()));
	    	ticket.setValidFrom(body.getValidFrom());
	    	ticket.setZone(Ticket.ZoneEnum.fromValue(body.getZone().getValue()));
	    	DataService.ticketList.add(ticket);
	    	return ResponseEntity.ok().body(ticket);
    	}catch (Exception e) {
    		e.printStackTrace();
    		 return ResponseEntity.status(400).body(null); 
		}
       
    }

    @PostMapping("/validate")
    public ResponseEntity<Void> validateTicket(@ApiParam(value = "TicketValidationRequest object that needs to validated" ,required=true )  @Valid @RequestBody TicketValidationRequest body) {
       List<Ticket> ticketList = DataService.ticketList;
       boolean valid=false;
       try {
	
	       for(Ticket ticket:ticketList) {
	    	   if(ticket.getId().longValue()==body.getTicketId().longValue()
	    			   && ticket.getDisabled().equals(body.getDisabled())
	    			   && ticket.getStudent().equals(body.getStudent())
	    			   && ticket.getZone().getValue().equals(body.getZone().getValue())) {
	    		   SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    	        long startDateTime = dateFormat.parse(ticket.getValidFrom()).getTime();
	    	        long endDateTime = dateFormat.parse(body.getDate()).getTime();
	    	        long diff =endDateTime - startDateTime;
	    	        Ticket.ValidForEnum type=  ticket.getValidFor();
	    	        long onehour = 1000*60*60;
	    		   if( (type.equals(Ticket.ValidForEnum._1H) && diff<onehour)
	    				   || (type.equals(Ticket.ValidForEnum._1D) && diff<onehour*24)
	    				   || (type.equals(Ticket.ValidForEnum._30D) && diff<onehour*24*30)
	    				   || (type.equals(Ticket.ValidForEnum._1Y) && diff<onehour*24*30*365) ) {
	    			   valid=true;
	    	    		   
	    		   }
	    	   }
	    	}
	   	
	   	} catch (Exception e) {
	   		e.printStackTrace();
	   	}
       if(valid) {
    	   return ResponseEntity.ok().body(null);
       }else {
    	   return ResponseEntity.status(403).body(null); 
       }
    }
}
