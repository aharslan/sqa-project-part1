package de.rwth.swc.sqa;


import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import de.rwth.swc.sqa.model.Customer;
import de.rwth.swc.sqa.model.DiscountCard;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class CustomerTest {
	private static List<Customer> customerList = new ArrayList<Customer>();

    @LocalServerPort
    int serverPort;

    @BeforeEach
    public void setup() {
        RestAssured.port = serverPort;
    }


    @Test
    @Order(1)
    public void addCustomerTest() {
    	 //400
        Map<String, Object> parms1 = new HashMap<String, Object>();
        parms1.put("birthdate", "");
        parms1.put("disabled", true);
        RestAssured.given().header("content-Type", "application/json").and()
        .body(parms1).when().post("/customers").then().statusCode(400);
    	//201
        Map<String, Object> parms = new HashMap<String, Object>();
    	parms.put("birthdate", "2000-01-01");
    	parms.put("disabled", false);
    	ValidatableResponse response = RestAssured.given().header("content-Type", "application/json").and()
        .body(parms).when().post("/customers").then().statusCode(201);
    	Customer customer = response.extract().as(Customer.class);
    	customerList.add(customer);
    	
        Map<String, Object> parms3 = new HashMap<String, Object>();
    	parms3.put("birthdate", "2001-01-01");
    	parms3.put("disabled", false);
    	response = RestAssured.given().header("content-Type", "application/json").and()
        .body(parms).when().post("/customers").then().statusCode(201);
    	customer = response.extract().as(Customer.class);
    	customerList.add(customer);

    }

    @Test
    @Order(2)
    public void addDiscountCardToCustomerTest() {
    	Long customerId =0L;
    	String path = "/customers/"+customerId+"/discountcards";
    	Map<String, Object> parms = new HashMap<String, Object>();
        //404
    	 customerId = 1L;
    	path = "/customers/"+customerId+"/discountcards";
    	parms.clear();
    	parms.put("customerId", customerId);
    	parms.put("id", 1L);
    	parms.put("type", 1);
    	parms.put("validFrom", "2022-01-01");
    	parms.put("validFor", DiscountCard.ValidForEnum._30D);
        RestAssured.given().header("content-Type", "application/json").and()
        .body(parms).when().post(path).then().statusCode(404);
    	
    	//400
        customerId = customerList.get(0).getId();
        path = "/customers/"+customerId+"/discountcards";
        parms.clear();
    	parms.put("customerId", customerId);
    	parms.put("type", 1);
    	parms.put("validFrom", "2022-01-01");
    	parms.put("validFor", DiscountCard.ValidForEnum._30D);
        RestAssured.given().header("content-Type", "application/json").and()
        .body(parms).when().post(path).then().statusCode(400);
        
        
      //201
    	customerId = customerList.get(0).getId();
    	path = "/customers/"+customerId+"/discountcards";
    	parms.clear();
    	parms.put("customerId", customerId);
    	parms.put("id", 1L);
    	parms.put("type", 1);
    	parms.put("validFrom", "2022-01-01");
    	parms.put("validFor", DiscountCard.ValidForEnum._30D);
        RestAssured.given().header("content-Type", "application/json").and()
        .body(parms).when().post(path).then().statusCode(201);
        
        //409 repeat conflict
        customerId = customerList.get(0).getId();
        path = "/customers/"+customerId+"/discountcards";
    	parms.clear();
    	parms.put("customerId", customerId);
    	parms.put("id", 1L);
    	parms.put("type", 1);
    	parms.put("validFrom", "2022-01-01");
    	parms.put("validFor", DiscountCard.ValidForEnum._30D);

        RestAssured.given().header("content-Type", "application/json").and()
        .body(parms).when().post(path).then().statusCode(409);


    }

    @Test
    @Order(3)
    public void getCustomerDiscountCardTest() {
    	Long customerId =0L;
    	String path = "/customers/"+customerId+"/discountcards";
        //400
    	path = "/customers/"+customerId+"/discountcards";
        RestAssured.given().when().get(path).then().statusCode(400);
    	
    	//404
        customerId = 11111L;
        path = "/customers/"+customerId+"/discountcards";
        RestAssured.given().when().get(path).then().statusCode(404);

        
        
      //201
    	customerId = customerList.get(0).getId();
    	path = "/customers/"+customerId+"/discountcards";
        RestAssured.given().when().get(path).then().statusCode(200);



    }
}






