package de.rwth.swc.sqa;


import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;

import de.rwth.swc.sqa.model.Ticket;
import io.restassured.RestAssured;
import io.restassured.response.ValidatableResponse;

@SpringBootTest(webEnvironment = RANDOM_PORT)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class TicketTest {
	private static List<Ticket> ticketList = new ArrayList<Ticket>();

    @LocalServerPort
    int serverPort;

    @BeforeEach
    public void setup() {
        RestAssured.port = serverPort;
    }


    @Test
    @Order(1)
    public void buyTicketTest() {
    	 //400//no birthdate
        Map<String, Object> parms1 = new HashMap<String, Object>();
        parms1.put("validFor", "1h");//
        parms1.put("zone", "A");
        parms1.put("student", true);
        parms1.put("discountCard", true);
        parms1.put("disabled", true);
        parms1.put("validFrom", "2022-05-20 10:00:00");
        RestAssured.given().header("content-Type", "application/json").and()
        .body(parms1).when().post("/tickets").then().statusCode(400);
        
    	//200
        parms1.clear();
        parms1.put("birthdate", "1992-01-01");
        parms1.put("validFor", "1d");
        parms1.put("zone", "A");
        parms1.put("student", true);
        parms1.put("discountCard", true);
        parms1.put("disabled", true);
        parms1.put("validFrom", "2022-05-20 10:00:00");
        ValidatableResponse response = RestAssured.given().header("content-Type", "application/json").and()
        .body(parms1).when().post("/tickets").then().statusCode(200);
        Ticket ticket = response.extract().as(Ticket.class);
        ticketList.add(ticket);
    	
    	
        parms1.clear();
        parms1.put("birthdate", "1992-01-01");
        parms1.put("validFor", "30d");
        parms1.put("zone", "A");
        parms1.put("student", true);
        parms1.put("discountCard", true);
        parms1.put("disabled", true);
        parms1.put("validFrom", "2022-05-20 10:00:00");
        response = RestAssured.given().header("content-Type", "application/json").and()
        .body(parms1).when().post("/tickets").then().statusCode(200);
        ticket = response.extract().as(Ticket.class);
        ticketList.add(ticket);
       

    }

    @Test
    @Order(2)
    public void validateTicketTest() {
    	String path = "/tickets/validate";
    	Long ticketId =0L;
    	Map<String, Object> parms = new HashMap<String, Object>();
        //400 //lacking info
    	parms.clear();
    	parms.put("ticketId", ticketId);
        RestAssured.given().header("content-Type", "application/json").and()
        .body(parms).when().post(path).then().statusCode(400);
    	
      //403 //ticketid incorrect
    	parms.clear();
    	parms.put("ticketId", ticketId);
    	parms.put("birthdate", "1992-01-01");
    	parms.put("zone", "A");
    	parms.put("student", true);
    	parms.put("disabled", true);
    	parms.put("date", "2022-05-22 10:00:00");
        RestAssured.given().header("content-Type", "application/json").and()
        .body(parms).when().post(path).then().statusCode(403);
        
    	//403//time incorrect
        ticketId = ticketList.get(0).getId();
        parms.clear();
    	parms.put("ticketId", ticketId);
    	parms.put("birthdate", "1992-01-01");
    	parms.put("zone", "A");
    	parms.put("student", true);
    	parms.put("disabled", true);
    	parms.put("date", "2022-05-22 10:00:00");
        RestAssured.given().header("content-Type", "application/json").and()
        .body(parms).when().post(path).then().statusCode(403);
        
        
      //200//correct
        ticketId = ticketList.get(0).getId();
        parms.clear();
    	parms.put("ticketId", ticketId);
    	parms.put("birthdate", "1992-01-01");
    	parms.put("zone", "A");
    	parms.put("student", true);
    	parms.put("disabled", true);
    	parms.put("date", "2022-05-20 12:00:00");
        RestAssured.given().header("content-Type", "application/json").and()
        .body(parms).when().post(path).then().statusCode(200);
        


    }

}






